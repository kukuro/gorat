package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"syscall"
	"unsafe"
)

type Bot struct {
	id     string
	ip     string
	status string
}

func get_wan_ip() string {
	resp, err := http.Get("http://myexternalip.com/raw")
	if err != nil {
		os.Exit(1)
	}
	defer resp.Body.Close()
	if ip, err := ioutil.ReadAll(resp.Body); err == nil {
		// remove the "\n" at the end
		return strings.Replace(string(ip), "\n", "", -1)
	} else {
		return ""
	}
}

func generate_uuid() string {
	id, err := getwindowsmachineid()
	if err == nil {

	}
	// MD5 hash the macaddress
	hasher := md5.New()
	hasher.Write([]byte(id))
	return hex.EncodeToString(hasher.Sum(nil))
}

func getwindowsmachineid() (guid string, err error) {
	var h syscall.Handle
	err = syscall.RegOpenKeyEx(syscall.HKEY_LOCAL_MACHINE, syscall.StringToUTF16Ptr(`SOFTWARE\Microsoft\Cryptography`), 0, syscall.KEY_READ, &h)
	if err != nil {
		return
	}
	defer syscall.RegCloseKey(h)
	var typ uint32
	var buf [74]uint16 // len = len(`{GUID-BLAH-BLAH}`) * 2
	n := uint32(len(buf))
	err = syscall.RegQueryValueEx(h, syscall.StringToUTF16Ptr("MachineGuid"), nil, &typ, (*byte)(unsafe.Pointer(&buf[0])), &n)
	if err != nil {
		return
	}
	guid = syscall.UTF16ToString(buf[:])
	return
}

func main() {
	fmt.Println("ID: ", generate_uuid())
}
