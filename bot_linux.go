package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"
)

type Bot struct {
	id     string
	ip     string
	status string
}

func get_wan_ip() string {
	resp, err := http.Get("http://myexternalip.com/raw")
	if err != nil {
		os.Exit(1)
	}
	defer resp.Body.Close()
	if ip, err := ioutil.ReadAll(resp.Body); err == nil {
		// remove the "\n" at the end
		return strings.Replace(string(ip), "\n", "", -1)
	} else {
		return ""
	}
}

func generate_uuid() string {
	// Get all interfaces
	interfaces, err := net.Interfaces()
	if err != nil {
		return ""
	}

	var macaddress net.HardwareAddr

	// Test interfaces' macaddresses (can't be empty)
	for _, inter := range interfaces {
		if len(inter.HardwareAddr) != 0 {
			macaddress = inter.HardwareAddr
		}
	}

	// MD5 hash the macaddress
	hasher := md5.New()
	hasher.Write(macaddress)

	return hex.EncodeToString(hasher.Sum(nil))
}

func main() {
	fmt.Println("ID: ", generate_uuid())
}
